import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import { Alert, Col, Container, Row, Label, Input } from 'reactstrap';
import styled from 'styled-components';

// should be in utils or smth
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this, args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

function App() {

  const [userData, setUsersData] = useState([]);
  const [searchFilter, setFilterText] = useState("");

  useEffect(async () => {
    fetch("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json", {
      mode: 'cors',
      headers: {
      },
    })
      .then((res) => res.json())
      .then((listOfUsers) => {
        listOfUsers.sort((a, b) => (a.last_name > b.last_name) ? 1 : -1)
        setUsersData(listOfUsers);
      })
  }, []);

  const onCheckboxChange = (index) => {
    let newList = userData;
    newList[index].checked = !newList[index].checked
    setUsersData(newList)
  }

  const handleSearch = debounce((e) => {
    setFilterText(e.target.value.toUpperCase())
  }, 250)


  console.log(searchFilter)
  let displayUsers = userData.filter((user) => {
    if (searchFilter == "")
      return true;
    // as simple as passible, typoes prevention would be cool
    if (user.first_name.toUpperCase().includes(searchFilter))
      return true;
    if (user.last_name.toUpperCase().includes(searchFilter))
      return true;
    return false;
  })



  const userList = displayUsers.map((singleUser, index) => {
    /* egxample singleUser:
      avatar: "https://robohash.org/fugiatautemodit.png?size=50x50&set=set1"
      email: "skydd0@prnewswire.com"
      first_name: "Suzie"
      gender: "Female"
      id: 1
      last_name: "Kydd"
    */
    return (
      <Col lg={4} md={6} xs={12}>
        <UserProfile key={singleUser.id}>
          <Row>
            <Col xs={3}>
              {singleUser.avatar ?
                <img src={singleUser.avatar}></img>
                : <span>No pic :) </span>}
              {/* it could be a nicer placeholder */}
            </Col>
            <NamesDiv>
              {/* snake case python users be like: ye, i am gonna do it in an outside api. disgusting */}
              <span>{singleUser.first_name}{' '}</span>
              <span>{singleUser.last_name}</span>
            </NamesDiv>
            <Col xs={3}>
              <BigCheckbox type="checkbox" checked={singleUser.checked} onClick={() => onCheckboxChange(index)} />
            </Col>
          </Row>
        </UserProfile>
      </Col>
    );
  });


  return (<>
    <Container>
      <SearchSpacing>
        <Label>
          Search:
        </Label>
        <Input onChange={handleSearch} />
      </SearchSpacing>
    </Container>
    <Container>
      <Row>
        {userList}
      </Row>
    </Container>
  </>
  );
}
export default App;

{/* i know alerts are not used for that, but they look cool and this is a prototype, ok?  */ }
const UserProfile = styled(Alert)`
min-height:90px;
`

const SearchSpacing = styled.div`
margin:30px 0px;
`

const BigCheckbox = styled.input`
height:40px;
width :40px;
margin-top:5px;
`

const NamesDiv = styled(Col)`
/* how to center text in div */
vertical-align: middle;
`


